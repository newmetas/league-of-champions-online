<?PHP

require_once ( "init.php" );
require_once ( "functions.php" );
require_once ( "../includes/functions-json.php" );

?>

<head>

	<?PHP

	addScriptPopupCard();

	?>

	<style>
		hr {color:sienna;}
		p {margin-left:20px;}
		body {
			background: #e3d59c;
			font-family: Georgia, serif;
		}

		.champion-card {
			background: #FFF8CC;
			color: #B62;
			border: 1px #FEA solid;
			width: 480px;
			max-width: 480px;
			display: inline-block;
			padding: 12px;
			margin: 12px;
		}

		.champion-card.single {
			margin: 0px;
		}

		.champion-card .submit {
			position: absolute;
			margin-left: 380px;
			width: 90px;
			height: 40px;
			display: none;
			background: none;
			border: none;
			font-size: 20px;
			color: #EC8;
		}

		.champion-card .submit:hover {
			background: #FFBB11;
			color: #662200;
			border: 1px #FFDD88 solid;
			outline: 1px solid #902000;
		}

		.champion-card:hover .submit {
			display: inline;
		}

		.champion-card .label {
			font-family: Verdana, sans-serif;
		}

		.champion-card .field {
			font-family: Verdana, sans-serif;
			border: 1px #FDA solid;
			outline: none;
			background: #FFFCE4;
			color: #940;
			padding: 8px;
		}

		.champion-card textarea.big-field {
			width: 100%;
			max-width: 100%;
			min-height: 80px;
		}

		.champion-card textarea.big-field.values {
			min-height: 50px;
			font-size: 10px;
			border: inherit;
			background: inherit;
			color: #C82;
		}

		.champion-card textarea.dev_notes {
			min-height: 160px;
		}

		.champion-card .field.header-field {
			font-family: Georgia, serif;
			border: /** / inherit /**/ 1px #FFF8CC solid /**/;
			background: inherit;
			width: 100%;
		}

		.champion-card .field:focus {
			border: 1px #EDC95D solid;
			background: #FFFDF5;
		}

		.champion-card .field:hover {
			background: #FFFDF5;
		}

		.log {
			font-family: Consolas, monospace;
			font-size: 12px;
			border: 1px #EDC95D solid;
			margin: 10px;
			padding: 10px;
		}

	</style>
</head>

<body>

<?PHP

if( isset( $_POST["submit"] ) ) {

	updateChampionData( $_POST["id"], $_POST );

}

if( isset( $_GET["champ"] ) ) {

	$id = $_GET["champ"];

	if( $id <= 0 ) {
		die( "no unit #$id" );
	}

	$data = getDataSingle( "champions", $id );

	drawChampCard( $data, "single" );

} else {

	$data = getData( "champions" );

	foreach ( $data as $i => $o ) {
		drawChampCard( $o, "multi" );
	}

}

drawLog();

# # #

function drawChampCard( $o, $css_class="" ) {

	global $curData;
	$curData = $o;

	echo "<div class='champion-card $css_class'>";
	echo "<form method='POST'>"; {

		echo "<input type='hidden' name='id' value='$o->id'/>";

		echo "<input class='submit' type='submit' name='submit' value='save this'/>";

		drawChampCardHeaderField( "name", 40 );

		drawChampCardHeaderField( "slug", 12 );

		drawChampCardHeaderField( "slogan", 16, true );

		nl();

		drawChampCardSmallField( "health" );

		drawChampCardSmallField( "attack" );

		drawChampCardSmallField( "speed" );

		nl();

		nl();

		drawChampAbilityField( "skill" );

		drawChampAbilityField( "defence" );

		drawChampAbilityField( "passive" );

		drawChampBigField( "story" );

		nl();

		nl();

		drawChampBigField( "dev_notes" );

		echo "</form>";
		echo "<a href=\"#\" onClick=\"MyWindow=popupChampionCard($o->id);\">linky....</a>";

	}
	echo "</div>";
	

}

function drawChampCardHeaderField( $key, $size, $area=false ) {

	global $curData;
	$val = $curData->$key;
	$label = ucwords( str_replace("_"," ",$key) );

	$size .= "px";

	if( $area ) {
		echo "<label class='label'><textarea class='field header-field' style='font-size: $size;' name='$key'>$val</textarea></label>";
	} else {
		echo "<label class='label'><input class='field header-field' style='font-size: $size; font-weight: bolder;' type='text' name='$key'' value='$val'></label>";
	}
	echo "</br>";

}

function drawChampCardSmallField( $key ) {

	global $curData;
	$val = $curData->$key;
	$label = ucwords( str_replace("_"," ",$key) );

	#echo "<$tag>$key: $val</$tag><br>";
	echo "<label class='label'>$label:<input class='field small-field' type='text' name='$key'' value='$val'></label>";
	echo "</br>";

}

function drawChampAbilityField( $key ) {

	global $curData;
	$desi = $key."_description";
	$vali = $key."_values";
	$des = $curData->$desi;
	$val = $curData->$vali;
	$label = ucwords( str_replace("_"," ",$key) );

	echo "<label class='label'>$label:<textarea class='field big-field $desi' name='$desi'>$des</textarea></label><textarea class='field big-field values $vali' name='$vali'>$val</textarea>";
	echo "</br>";

}

function drawChampBigField( $key ) {

	global $curData;
	$val = $curData->$key;
	$label = ucwords( str_replace("_"," ",$key) );

	echo "<label class='label'>$label:<textarea class='field big-field $key' name='$key'>$val</textarea></label>";
	echo "</br>";

}

?>

</body>