<?PHP

require_once ( "conf.php" );

$mysql = mysql_connect(MySQL_HOST,MySQL_USER,MySQL_PASS);

if (!$mysql) die('Could not connect: ' . mysql_error());

@mysql_select_db(MySQL_DB) or die( "Unable to select database");

mysql_set_charset('utf8', $mysql);

# # #

function uninit() {

    global $mysql;

    mysql_close($mysql);

}

# # #

function __isset( $v ) {

  return ( isset( $_GET[ $v ] ) || isset( $_POST[ $v ] ) );

}

function __get( $v ) {

  if( isset( $_GET[ $v ] ) ) {
    return $_GET[ $v ];
  } else if( isset( $_POST[ $v ] ) ) {
    return $_POST[ $v ];
  } else {
    doError( "couldn't find var $v" );
  }

}

?>