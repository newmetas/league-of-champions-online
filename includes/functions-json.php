<?php

require_once ( "pretty-json.php" );

global $html;
//$html = __isset( "html" ) ? __get( "html" ) : false;
$html = __isset( "html" );

function wrapJsonInList( $json ) {

  return wrapJsonIn( $json, "list");

}

function wrapJsonInData( $json ) {

  return wrapJsonIn( $json, "data");

}

function wrapJsonIn( $json, $wrapName ) {

  return '{"' . $wrapName . '":' . $json . '}';

}

function printToJson( $data ) {

  printJson( json_encode( $data ) );

}

function printJson( $json ) {

  global $html;

  echo _format_json( $json, $html );

}

function toJson( $data ) {

  global $html;

  return _format_json( $json, $html );

}

?>